    # WC Schedule Planner - plan your next semester easily
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bottle import *
import cherrypy
from ..model import transcoder
from ..model import musiclibrary
from gevent import monkey; monkey.patch_all()

import time

class MusicServerApp(Bottle):
		def __init__(self,originalDataURL):
				super(MusicServerApp, self).__init__()
				self.myTranscoder=transcoder.Transcoder()
				self.myLibrary=musiclibrary.MusicLibrary(["/Users/avinashvakil/Music/iTunes/iTunes Music/Music"],'lib.lib')
				self.initializeRoutes()

		def initializeRoutes(self):
				@self.route('/static/<filename:path>')
				def server_static(filename):
						return static_file(filename, root='static')
						
				@self.route('/<songID>.mp3')
				def listclasses(songID):
						song=self.myLibrary.songIDs[int(songID)]
						songLocation=song.filename
						return self.myTranscoder.streamTrack(songLocation)
						
				@self.route('/stream')
				def stream():
					print response
					print 'start'
					for x in xrange(10000):
						yield '\n'+str(x)
					time.sleep(2)
					yield 'done'