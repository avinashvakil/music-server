    # WC Schedule Planner - plan your next semester easily
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.


from hsaudiotag import auto
import os
import cPickle as pickle

class MusicLibrary(object):
	def __init__(self,baseDirs,libraryStorageFile):
		self.baseDirs=baseDirs
		self.libraryStorageFile=libraryStorageFile
		
		#attempt to load the library file...
		if not os.path.exists(libraryStorageFile):
			self.reloadLibrary()
		else:
			libraryFile=open(libraryStorageFile,'r')
			library=pickle.load(libraryFile)
			libraryFile.close()
			print "loaded existing library in",libraryStorageFile
			self.library=library
			
		infoFile=open(libraryStorageFile+'.libinfo','r')
		oldBaseDirs=pickle.load(infoFile)
		infoFile.close()
		print "checking if dirs changed",oldBaseDirs
		if self.baseDirs!=oldBaseDirs:
			print "library not correct, reloading..."
			self.reloadLibrary()
			

			
		print len(self.library), "tracks have been loaded..."
		print filter(lambda x: x.bitrate>400,self.library)
		self.songIDs={}
		for asong in self.library:
			self.songIDs[asong.trackUUID]=asong
		print self.songIDs
		
	def reloadLibrary(self):
		self._reloadMusicLibrary(self.baseDirs)

		
	#private methods for library class.
	def _reloadMusicLibrary(self,baseDirs):
		tracks=[]
		for musicdir in baseDirs:
			print "Listing directory...", musicdir
			musicList=listAllFiles(musicdir)
			print "Reading music...", len(musicList)
			for asong in musicList:
				theTag=auto.File(asong)
				if theTag.valid==True:
					tracks.append( Song(asong,theTag) )
				else:
					print asong,"is not a song"
				print musicList.index(asong)/float(len(musicList))
		musicFile=open(self.libraryStorageFile,'w')
		pickle.dump(tracks,musicFile)
		musicFile.close()
		
		infoFile=open(self.libraryStorageFile+'.libinfo','w')
		pickle.dump(self.baseDirs,infoFile)
		infoFile.close()

		self.library=tracks
		


class Song(object):
	def __init__(self,filename,tags):
		self.filename=filename
		for tagType in ["title","artist","album","genre","track","year","comment","duration","bitrate","sample_rate","size","audio_offset"]:
			try:
				self.__dict__[tagType]=tags.__dict__[tagType]
			except:
				self.__dict__[tagType]=""
		self.trackUUID=id(self)
	
	def __repr__(self):
		return str(self.__dict__)+"\n"
			
def listAllFiles(directoryToList):
	outputfiles=[]
	for root,dirs,files in os.walk(directoryToList):
		for file in files:
			outputfiles.append(os.path.join(root,file))
	return outputfiles

